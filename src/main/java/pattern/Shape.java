package pattern;

public interface Shape {
    void draw();
    ShapeType getShapeType();
}
