package pattern;

public class ShapeFactory {

    //use getShape method to get object of type shape
    public Shape getShape(ShapeType shapeType) throws EnumConstantNotPresentException, IllegalAccessException, InstantiationException {
        if(shapeType == null){
            return null;
        }

        return shapeType.getType().newInstance();
    }
}
