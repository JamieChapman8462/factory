package pattern;

public enum ShapeType {
    CIRCLE(Circle.class),
    SQUARE(Square.class),
    RECTANGLE(Rectangle.class);

    ShapeType(Class<? extends Shape> circleClass) {
        type = circleClass;
    }

    public Class<? extends Shape> getType() {
        return type;
    }

    private Class<? extends Shape> type;
}
